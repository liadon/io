package managementStrategy.interfaces;

import producedepartment.bean.ProduceHistory;
import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;

public interface IManagementStrategy {
	public int getAdvertismentFunds(ProduceHistory prodHist,SalesHistory salesHistory,AdvertisingHistory advHistory);
}
