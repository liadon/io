package managementStrategy.impl;

import producedepartment.bean.ProduceHistory;
import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;
import managementStrategy.interfaces.IManagementStrategy;

public class NoAdvertismentFundsStrategy implements IManagementStrategy {

	@Override
	public int getAdvertismentFunds(ProduceHistory prodHist, SalesHistory salesHistory,
			AdvertisingHistory advHistory) {
		return 0;
	}
}
