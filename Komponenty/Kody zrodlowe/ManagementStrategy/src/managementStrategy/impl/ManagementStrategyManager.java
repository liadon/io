package managementStrategy.impl;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import managementStrategy.interfaces.IManagementStrategy;

public class ManagementStrategyManager {
	@Getter
	private static String[] strategiesNames = {"NoAdvertismentFunds", "1000AdvertismentFunds"};
	private static Map<String,IManagementStrategy> strategies = new HashMap<String,IManagementStrategy>();
	static {
		strategies.put("NoAdvertismentFunds", new NoAdvertismentFundsStrategy());
		strategies.put("1000AdvertismentFunds", new ConstAdvertismentFundsStrategy(1000));
	}

	@Getter
	private IManagementStrategy currentStrategy;
	@Getter
	private String currentStrategyName;
	public ManagementStrategyManager() {
		currentStrategyName = "1000AdvertismentFunds";
		currentStrategy = strategies.get(currentStrategyName);
	}
	
	/**
	 * Zmienia strategię Zarządu
	 * @param name
	 */
	public void changeStr(String name){
		currentStrategy = strategies.get(name);
		currentStrategyName = name;
	}

}
