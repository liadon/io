package managementStrategy.impl;

import producedepartment.bean.ProduceHistory;
import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;
import managementStrategy.interfaces.IManagementStrategy;

public class ConstAdvertismentFundsStrategy implements IManagementStrategy {

	private int funds;

	public ConstAdvertismentFundsStrategy(int funds) {
		this.funds = funds;
	}
	
	@Override
	public int getAdvertismentFunds(ProduceHistory prodHist,
			SalesHistory salesHistory, AdvertisingHistory advHistory) {
		return funds;
	}

}
