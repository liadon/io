package data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import producer.impl.Producer;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Advertisment {
	private Producer producer;
	private long funds;
}
