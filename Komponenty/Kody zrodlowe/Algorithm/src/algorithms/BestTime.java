package algorithms;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import data.Advertisment;

import producer.bean.OrderInfo;
import producer.impl.Producer;
import product.impl.Product;
/**
 * Algorytm wyszukukacy producentów po najwczesniejszej
 * dacie produkcji
 * @author Przemek Pioro
 *
 */
public class BestTime implements IAlgorithm {

	private Date bestTime = null;
	private Producer producer = null;
	private long advTotal;
	@Override
	public Producer find(Map<Producer, OrderInfo> data) {
		bestTime = null;
		producer = null;
		for(Entry<Producer,OrderInfo> entry : data.entrySet()){
			if(bestTime == null){
				bestTime = entry.getValue().getFinishDate();
			}
			else {
				if(entry.getValue().getFinishDate().before(bestTime)){
					bestTime = entry.getValue().getFinishDate();
					producer = entry.getKey();
				}
			}
		}
		return producer;
	}
	@Override
	public Producer find(Map<Producer, Long> ads,
			Map<Producer, OrderInfo> data) {
		// TODO Auto-generated method stub
		bestTime = null;
		producer = null;
		advTotal=0;
		for(Entry<Producer,OrderInfo> entry : data.entrySet()){
			if(bestTime == null){
				bestTime = entry.getValue().getFinishDate();
				if(ads.get(entry.getKey())!= null)
					advTotal = ads.get(entry.getKey());
			}
			else if(entry.getValue().getFinishDate().before(bestTime)){
				bestTime = entry.getValue().getFinishDate();
				producer = entry.getKey();
				
			}else if(entry.getValue().getFinishDate().equals(bestTime)&&ads.get(entry.getKey())>advTotal){
				advTotal = ads.get(entry.getKey());
				bestTime = entry.getValue().getFinishDate();
				producer = entry.getKey();
			}
		}
		return producer;
	}

}
