package algorithms;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;



import data.Advertisment;

import producer.bean.OrderInfo;
import producer.impl.Producer;
import product.impl.Product;
import algorithms.IAlgorithm;
/**
 * Algorytm wyszukukacy producentów po najlepszej cenie
 * @author Przemek Pioro
 *
 */
public class BestPrice implements IAlgorithm {
	private long price = 0;//zmienic to na long albo BigDecimal
	private Producer producer = null;
	private long advTotal = 0;
	@Override
	public Producer find(Map<Producer, OrderInfo> data) {
		price = 0;
		producer = null;
		for (Entry<Producer, OrderInfo> entry : data.entrySet()) {
			if(producer == null){
				price = entry.getValue().getCost();
				producer = entry.getKey();
			}else{
				if(entry.getValue().getCost() < price){
					price = entry.getValue().getCost();
					producer = entry.getKey();
				}	
			}
		}
		return producer;
	}

	@Override
	public Producer find(Map<Producer, Long> ads,
			Map<Producer, OrderInfo> data) {
		price = 0;
		advTotal = 0;
		producer = null;
		for (Entry<Producer, OrderInfo> entry : data.entrySet()) {
			if(producer == null){
				price = entry.getValue().getCost();
				producer = entry.getKey();
				if(ads.get(entry.getKey())!= null)
					advTotal = ads.get(entry.getKey());
			}else if(entry.getValue().getCost() < price){
				price = entry.getValue().getCost();
				producer = entry.getKey();
				System.out.println("|||||||||||||decyduje tylko cena");
			}else if(entry.getValue().getCost() == price && ads !=null&&ads.get(entry.getKey())!=null && ads.get(entry.getKey())>advTotal){
				System.out.println("|||||||||||||decyduje reklama"+producer.getCompanyName()+ads.get(producer));
				advTotal = ads.get(entry.getKey());
				price = entry.getValue().getCost();
				producer = entry.getKey();
				System.out.println("|||||||||||||decyduje reklama"+producer.getCompanyName()+ads.get(producer));
			}
		}
		//log.info("algorytm zwraca");
		return producer;
	}

}
