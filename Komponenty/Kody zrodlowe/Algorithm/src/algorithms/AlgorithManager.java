package algorithms;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import producer.bean.OrderInfo;
import producer.impl.Producer;
/**
 * Manager dla algorytmow
 * @author Przemek Pioro
 *
 */
@Data
public class AlgorithManager {
	private static IAlgorithm currentAlgorithm;
	private static String[] algorithmsName = {"BestPrice", "BestTime"};
	private static Map<String,IAlgorithm> algorithm = new HashMap<String,IAlgorithm>();
	static {
		algorithm.put("BestPrice",new BestPrice());
		algorithm.put("BestTime", new BestTime());
		currentAlgorithm = algorithm.get("BestPrice");
	}
	
	/**
	 * Zwraca obecnie używany algorytm
	 * @return
	 */
	public static IAlgorithm getCurrentUsedAlgorithm(){
		return currentAlgorithm;
	}

	/**
	 * Zwraca nazwy obecnie uzywanych algorytmów
	 * @return
	 */
	public static String[] getAllAlgorithm(){
		return algorithmsName;
	}
	
	/**
	 * Analizuje zamówienia i wybiera najlepsze
	 * @param negotiationInfo
	 * @return
	 */
	public static Producer analize(Map<Producer, OrderInfo> negotiationInfo) {
		return currentAlgorithm.find(negotiationInfo);	
	}
	
	public static Producer analizeWithAds(Map<Producer, Long> ads,Map<Producer,OrderInfo> data){
		return currentAlgorithm.find(ads,data);	
	}
	
	/**
	 * Zmienia algorytm negocjacji
	 * @param name
	 */
	public static void changeAlg(String name){
		currentAlgorithm = algorithm.get(name);
	}
}
