package algorithms;

import java.util.Map;
import java.util.Set;

import data.Advertisment;

import producer.bean.OrderInfo;
import producer.impl.Producer;
import product.impl.Product;
/**
 * Interfejs implementacji algorytmow
 * @author Przemek Pioro
 *
 */
public interface IAlgorithm {
	Producer find(Map<Producer,OrderInfo> data);
	Producer find(Map<Producer, Long> ads,Map<Producer,OrderInfo> data);
}
