package product.interfaces;

import product.enums.Category;

/**
 * Interfejs udostepniany przez produktu 
 * dla innych komponentow
 * @author Witold Muszynski
 *
 */

public interface IProduct {
	public int getCount();
	public String getName();
	public Category getCategory();
}
