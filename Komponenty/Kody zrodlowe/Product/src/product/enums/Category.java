package product.enums;

/**
 * Enum z informacja o kategoriach produktów/producentów
 * @author Przemek Pióro
 */

public enum Category {
	Sawmill,PrintingHouse,PublishingHouse,Mine,PowerPlant;
}
