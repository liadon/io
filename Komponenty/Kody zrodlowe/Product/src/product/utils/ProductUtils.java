package product.utils;

import product.impl.Product;
/**
 * Utils dla komponentu Product
 * @author przemek
 *
 */
public class ProductUtils {
	/**
	 * Metoda sprawdzajaca czy 2 produktu sa rowne 
	 * jezeli chodzi o nazwa i kategorie
	 * @param p1
	 * @param p2
	 * @return
	 */
	  public static boolean equals(Product p1,Product p2){
		 if(p1.getName().equals(p2.getName())){
			 return true;
			//if(p1.getCategory().equals(p2.getCategory())){
			//	return true;
			//}
		 }
		return false;
	}
}
