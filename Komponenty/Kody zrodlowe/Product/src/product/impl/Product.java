package product.impl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import product.enums.Category;
import product.interfaces.IProduct;


/**
 * Encja dla produktu
 * @author Witold Muszynski
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product implements IProduct {
	private String name;
	private int count;
	private Category category;
	public Product(Product prod){
		name=  prod.getName();
		count = prod.getCount();
		category = prod.getCategory();
	}
	public Product(String name){
		this.name=  name;
		count = 0;
		//category = Category.
	}
	@Override
	public boolean equals(Object obj){
		Product prod = (Product)obj;
		if(prod.getName().equals(this.name))
			return true;
		return false;
		
	}
	@Override
	public int hashCode(){
		return name.hashCode();
	}
}
