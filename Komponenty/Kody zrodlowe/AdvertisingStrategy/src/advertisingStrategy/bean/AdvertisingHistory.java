package advertisingStrategy.bean;

import java.util.Date;
import java.util.LinkedList;

import lombok.Getter;
import product.impl.Product;

public class AdvertisingHistory {
	@Getter
	LinkedList<AdvertisingHistoryBean> history;

	public AdvertisingHistory() {
		history = new LinkedList<AdvertisingHistoryBean>();
	}
	/**
	 * Dodaje produkt do historii produkcji
	 * @param product
	 * @param date
	 */
	public void addAdvertisment(long amount,Product product, Date date) {
		history.addFirst(new AdvertisingHistoryBean(amount,product, date));
	}
}
