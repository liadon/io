package advertisingStrategy.bean;

import java.util.Date;

import product.impl.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisingHistoryBean {
	
	private long amount;	//ile na to wydano
	private Product product;//jaki produkt reklamowano
	private Date date;//kiedy podj�to kampani� 
}
