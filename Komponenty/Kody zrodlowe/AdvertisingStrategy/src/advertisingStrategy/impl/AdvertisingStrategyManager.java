package advertisingStrategy.impl;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;



public class AdvertisingStrategyManager {
	@Getter
	private IAdvertisingStrategy currentStrategy;
	@Getter
	private static String[] strategiesNames = {"EqualDistribution", "NaiveMaxProfit","FavourMostProfitable","PromoteLeastProfitable","PromoteNew"};
	private Map<String,IAdvertisingStrategy> strategies = new HashMap<String,IAdvertisingStrategy>();
	private String currentStrategyName;
	public AdvertisingStrategyManager(){
		strategies.put("EqualDistribution",new EqualDistributionStrategy());
		strategies.put("NaiveMaxProfit",new NaiveMaxProfitStrategy());
		strategies.put("FavourMostProfitable",new FavourMostProfitable());
		strategies.put("PromoteLeastProfitable",new PromoteLeastProfitableProductStrategy());
		strategies.put("PromoteNewProductsStrategy",new PromoteNewProductsStrategy());
		currentStrategyName = "EqualDistribution";
		currentStrategy = strategies.get(currentStrategyName);
	}
	
	/**
	 * Analizuje zamówienia i wybiera najlepsze
	 * @param negotiationInfo
	 * @return
	 */
	//public static Producer analize(Map<Producer,List<OrderInfo>> producers) {
	//	return currentStrategy.find(producers);	
	//}
	
	/**
	 * Zmienia strategie marketingowa
	 * @param name
	 */
	public void changeStr(String name){
		currentStrategy = strategies.get(name);
		currentStrategyName = name;
	}
	
	public String getCurrentStrategyName() {
		return currentStrategyName;
	}
}
