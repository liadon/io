package advertisingStrategy.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import product.impl.Product;

import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

public class EqualDistributionStrategy implements IAdvertisingStrategy {

	/*@Override
	public List<AdvertisingDecision> find(SalesHistory saleshist,
			AdvertisingHistory adhist, long funds) {
		List<AdvertisingDecision> retList = new LinkedList<AdvertisingDecision>();
		Set<Product> sales= saleshist.getProducts();
		for(Product prod:sales){
			retList.add(new AdvertisingDecision(prod,funds/sales.size()));
		}
		return retList;
	}*/

	@Override
	public List<AdvertisingDecision> find(SalesHistory saleshist,
			AdvertisingHistory adhist, long funds,List<Product> products) {
		List<AdvertisingDecision> retList = new LinkedList<AdvertisingDecision>();
		//Set<Product> sales= saleshist.getProducts();
		for(Product prod:products){
			retList.add(new AdvertisingDecision(prod,funds/products.size()));
		}
		return retList;
	}

}
