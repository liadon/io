package advertisingStrategy.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import product.impl.Product;


import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

public class NaiveMaxProfitStrategy implements IAdvertisingStrategy{


	@Override
	public List<AdvertisingDecision> find(SalesHistory saleshist,
			AdvertisingHistory adhist, long funds, List<Product> products) {
		List<AdvertisingDecision> result = new LinkedList<AdvertisingDecision>();
		result.add(new AdvertisingDecision(saleshist.getMaxProfitProduct(),funds));
		return result;
	}

}
