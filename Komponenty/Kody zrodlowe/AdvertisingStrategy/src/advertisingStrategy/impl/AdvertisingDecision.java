package advertisingStrategy.impl;

import product.impl.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisingDecision {
	private Product product;
	private long amount;
}
