package advertisingStrategy.impl;

import java.util.LinkedList;
import java.util.List;

import product.impl.Product;

import saledepartment.bean.SalesHistory;

import advertisingStrategy.bean.AdvertisingHistory;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

public class FavourMostProfitable implements IAdvertisingStrategy {

	@Override
	public List<AdvertisingDecision> find(SalesHistory saleshist,
			AdvertisingHistory adhist, long funds, List<Product> products) {
		List<Product> list = saleshist.getSortedByProfit();
		int size = list.size();
		long amount = funds;
		long step = (funds/2); 
		
		List<AdvertisingDecision> retlist = new LinkedList<AdvertisingDecision>();
		if(size>3){
			retlist.add(new AdvertisingDecision(list.get(0),funds/2));
			retlist.add(new AdvertisingDecision(list.get(1),funds/4));
			retlist.add(new AdvertisingDecision(list.get(2),funds/4));
		}else if (size>2){
			retlist.add(new AdvertisingDecision(list.get(0),2*funds/3));
			retlist.add(new AdvertisingDecision(list.get(1),funds/3));
		}else if(size==1){
			retlist.add(new AdvertisingDecision(list.get(0),funds));
		}
		return retlist;
	}



}
