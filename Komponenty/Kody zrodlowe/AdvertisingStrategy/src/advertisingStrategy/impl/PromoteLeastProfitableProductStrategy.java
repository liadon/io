package advertisingStrategy.impl;

import java.util.LinkedList;
import java.util.List;

import product.impl.Product;

import saledepartment.bean.SalesHistory;
import advertisingStrategy.bean.AdvertisingHistory;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

public class PromoteLeastProfitableProductStrategy implements IAdvertisingStrategy{

	@Override
	public List<AdvertisingDecision> find(SalesHistory saleshist,
			AdvertisingHistory adhist, long funds, List<Product> products) {
		List<AdvertisingDecision> result = new LinkedList<AdvertisingDecision>();
		result.add(new AdvertisingDecision(saleshist.getMinProfitProduct(),funds));
		return result;
	}

}
