package advrtisingStrategy.interfaces;

import java.util.List;

import product.impl.Product;


import advertisingStrategy.bean.AdvertisingHistory;
import advertisingStrategy.impl.AdvertisingDecision;


import saledepartment.bean.SalesHistory;

public interface IAdvertisingStrategy {
	List<AdvertisingDecision> find(SalesHistory saleshist,AdvertisingHistory adhist, long funds, List<Product> products);
}
