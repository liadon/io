/**
 * 
 */
package gui.components;

import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JTextArea;

import magazin.impl.Magazin;
import producer.impl.Producer;
import product.impl.Product;

@SuppressWarnings("serial")
/**
 * Okno wyświetlające stan magazynu
 * @author Martyna Wałaszewska
 *
 */
public class MagazinStateDialog extends JDialog {
    private javax.swing.JLabel producerLabel = new javax.swing.JLabel();
//    private javax.swing.JLabel produceDepartmentLabel = new javax.swing.JLabel();
    private javax.swing.JLabel saleDepartmentLabel = new javax.swing.JLabel();
//    private javax.swing.JScrollPane producePane = new javax.swing.JScrollPane();
    private javax.swing.JScrollPane salePane = new javax.swing.JScrollPane();
//    private javax.swing.JTextArea produceState = new javax.swing.JTextArea();
    private javax.swing.JTextArea saleState = new javax.swing.JTextArea();
    private Producer producer;
    
	public MagazinStateDialog (Frame owner, boolean modal){
		super(owner, modal);
		init();
	}
    
    public void setProducer(Producer producer) {
    	if(null == producer) return;
    	
    	this.producer = producer;
    	producerLabel.setText("Stan magazynu dla przedsi�biorstwa: " + this.producer.getName());

//    	Magazin produceMagazin = producer.getManagment().getProduceDepartment().magazin;
    	Magazin saleMagazin = producer.getManagment().getSaleDepartment().magazin;
//    	setState(produceState, produceMagazin);
    	setState(saleState, saleMagazin);    	
    }
	
    private void setState(JTextArea state, Magazin magazin) {
    	state.setText("");
		for(Product product : magazin.getMagazinState()) {
			StringBuilder entry = new StringBuilder();
			entry.append("Nazwa produktu: ");
			entry.append(product.getName());
			entry.append(" Kategoria: ");
			entry.append(product.getCategory());
			entry.append(" Ilosc: ");
			entry.append(product.getCount());
			entry.append("\n");
			state.append(entry.toString());
		}
		
	}

	private void init() {
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        
        producerLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        producerLabel.setText("Nie wybrano przedsi�biorstwa");
//        produceDepartmentLabel.setText("Stan magazynu produkcyjnego:");
        saleDepartmentLabel.setText("Stan magazynu sprzeda�y:");
 
//        jTextArea1.setColumns(20);
//        jTextArea1.setRows(5);
//        produceState.setFocusable(false);
        saleState.setFocusable(false);

//        producePane.setEnabled(false);
//        producePane.setAutoscrolls(true);
//        producePane.setViewportView(produceState);
        salePane.setEnabled(false);
        salePane.setAutoscrolls(true);
        salePane.setViewportView(saleState);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(producerLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
//                    .addComponent(produceDepartmentLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
//                    .addComponent(producePane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
                    .addComponent(saleDepartmentLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
                    .addComponent(salePane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE))
                .addContainerGap())
        );
                
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(producerLabel)
//                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
//                .addComponent(produceDepartmentLabel)
//                .addComponent(producePane, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saleDepartmentLabel)
                .addComponent(salePane, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }
}
