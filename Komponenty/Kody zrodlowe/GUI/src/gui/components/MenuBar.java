package gui.components;

import java.util.List;

import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


import producer.impl.Producer;

//import negotiator.Negotiator;
import negotiator.Negotiator2;


import lombok.Setter;
import negotiator.Negotiator;
import producer.impl.Producer;
/**
 * Pasek narzedzi
 * @author Witold Muszynski
 *
 */
public class MenuBar extends JMenuBar {
        
        
    private javax.swing.JMenu jMenuPlik;
    private javax.swing.JMenu jMenuPomoc;
    private javax.swing.JMenu jMenuEdycja;

	private JMenuItem jMenuItemShowLogs;
    private javax.swing.JMenuItem jMenuItemInfo;
    private javax.swing.JMenuItem jMenuItemPace;
    private javax.swing.JMenuItem jMenuItemHelp;
    private javax.swing.JMenuItem jMenuItemGenAll;
    private javax.swing.JMenuItem jMenuItemGenCat;
    private javax.swing.JMenuItem jMenuItemGenFirm;
    private javax.swing.JMenuItem jMenuItemEnd;
    private javax.swing.JMenuItem jMenuItemSales;
    
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    @Setter   
    private WaitTimeDialog waitTimeDialog;
	private Negotiator2 negotiator;//tu byl 1
	@Setter
	private HistoryDialog historyDialog;
	@Setter
	private SalesHistoryDialog salesDialog;
	private List<Producer> producers;
	private Console console = Console.getConsole();

        
        public MenuBar() {
                initializeHelpMenu();
        }

        public void setNegotiator(Negotiator2 negotiator) {
        	this.negotiator = negotiator;
        	console.setNegotiator(negotiator);
        }
        
        private void initializeHelpMenu() {
                        
                jMenuPlik = new javax.swing.JMenu();
                jSeparator1 = new javax.swing.JPopupMenu.Separator();
                jMenuItemShowLogs = new javax.swing.JMenuItem();
                jMenuItemGenAll = new javax.swing.JMenuItem();
                jMenuItemGenCat = new javax.swing.JMenuItem();
                jMenuItemGenFirm = new javax.swing.JMenuItem();
                jSeparator2 = new javax.swing.JPopupMenu.Separator();
                jMenuItemEnd = new javax.swing.JMenuItem();
                jMenuEdycja = new javax.swing.JMenu();
                jMenuItemPace = new javax.swing.JMenuItem();
                jMenuPomoc = new javax.swing.JMenu();
                jMenuItemInfo = new javax.swing.JMenuItem();
                jMenuItemHelp = new javax.swing.JMenuItem();
                jMenuItemSales = new javax.swing.JMenuItem();
                
                jMenuPlik.setText("Plik");
                
                jMenuPlik.add(jSeparator1);
 
                jMenuItemShowLogs.setText("Poka� konsol�");
                jMenuItemShowLogs.addActionListener(new java.awt.event.ActionListener() {
    	            public void actionPerformed(java.awt.event.ActionEvent evt) {
    	                console.setVisible(true);
    	            }
    	        });
                jMenuPlik.add(jMenuItemShowLogs);
                
                jMenuItemGenAll.setText("Generuj raport dla wszystkich firm");
                jMenuItemGenAll.addActionListener(new java.awt.event.ActionListener() {
    	            public void actionPerformed(java.awt.event.ActionEvent evt) {
    	                jMenuItemGenAllActionPerformed(evt);
    	            }
    	        });
                jMenuPlik.add(jMenuItemGenAll);

                jMenuItemGenCat.setText("Generuj raport dla firm z kategorii ...");
                jMenuItemGenCat.addActionListener(new java.awt.event.ActionListener() {
    	            public void actionPerformed(java.awt.event.ActionEvent evt) {
    	                jMenuItemGenCatActionPerformed(evt);
    	            }
    	        });
                jMenuPlik.add(jMenuItemGenCat);

                /*jMenuItemGenFirm.setText("Generuj raport dla firmy ...");
                jMenuPlik.add(jMenuItemGenFirm);*/
                jMenuPlik.add(jSeparator2);


                jMenuItemEnd.setText("Zakoncz");
                jMenuPlik.add(jMenuItemEnd);

                add(jMenuPlik);

                jMenuEdycja.setText("Edycja");

                jMenuItemPace.setText("Zmien tempo symulacji");
                jMenuItemPace.addActionListener(new java.awt.event.ActionListener() {
    	            public void actionPerformed(java.awt.event.ActionEvent evt) {
    	                jMenuItemPaceActionPerformed(evt);
    	            }
    	        });
                jMenuEdycja.add(jMenuItemPace);


                add(jMenuEdycja);

                jMenuPomoc.setText("Pomoc");

                jMenuItemInfo.setText("Informacje o programie");
                jMenuPomoc.add(jMenuItemInfo);

                jMenuItemHelp.setText("Plik pomocy");
                jMenuPomoc.add(jMenuItemHelp);

                add(jMenuPomoc);

              
        }
        
private void jMenuItemPaceActionPerformed(java.awt.event.ActionEvent evt) {
			waitTimeDialog.repaint();
			waitTimeDialog.setVisible(true);
}

private void jMenuItemGenAllActionPerformed(java.awt.event.ActionEvent evt) {
	historyDialog.setAreaText("");
	historyDialog.setLabelText("Raport produkcji dla wszystkich producent�w");
	
	producers = negotiator.getProducers();
		
	for(Producer p : producers){
		historyDialog.addProducerToArea(p);
	}
	
	historyDialog.repaint();
	historyDialog.setVisible(true);
}

private void jMenuItemGenCatActionPerformed(java.awt.event.ActionEvent evt) {
	historyDialog.setAreaText("");
	historyDialog.setLabelText("Raport produkcji dla wszystkich producent�w wg kategorii");
	
	producers = negotiator.getProducers();
		
	for(Producer p : producers){
		historyDialog.addProducerToArea(p);
	}
	
	historyDialog.repaint();
	historyDialog.setVisible(true);
}
        
}
