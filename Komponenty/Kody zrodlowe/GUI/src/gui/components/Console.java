package gui.components;

import java.awt.Dimension;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import lombok.Getter;
import lombok.Setter;
import negotiator.Negotiator2;
import negotiator.Transaction;

public class Console extends JDialog {
	static Console _instance;
	static Console getConsole() {
		if(null==_instance) {
			_instance = new Console();
		}
		return _instance;
	}
	
	@Setter
	private Negotiator2 negotiator;

	private JTextArea field = new JTextArea();
	private JLabel label = new JLabel("Konsola negocjatora:");
    private javax.swing.JScrollPane pane = new javax.swing.JScrollPane();
    
	private Console() {
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        
        field.setFocusable(false);
        field.setAutoscrolls(true);
        field.setEditable(false);
        pane.setEnabled(false);
        pane.setAutoscrolls(true);
        pane.setViewportView(field);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
                    .addComponent(pane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE))
                .addContainerGap())
        );
                
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(label)
                .addComponent(pane, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );
        pack();
        
        new ConsoleThread().start();

	}
	
    class ConsoleThread extends Thread {
    	public ConsoleThread() {
    		super();
    		setDaemon(true);
    	}
    	public void run() {
    			//setDaemon(true);
    			try {
    				while(true) {
    					sleep(1000);
    					if(null!=negotiator) {
    						field.setText("");
    						List<Transaction> history = negotiator.getTransactions();
    						for(Transaction t : history) {
    							field.append(t.getBuyer().getCompanyName() + " kupil od " +t.getSeller().getCompanyName() +
    									" produkt " + t.getProduct().getName() + " po cenie " + t.getPrice()+ "\n");
    						}
    						
    					}
    				}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
    	}
    }
}
