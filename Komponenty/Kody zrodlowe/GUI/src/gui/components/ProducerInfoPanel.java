package gui.components;

/*Przyjmiemy sobie, ze rok to 20 min jeden miesiac to 2 min, tydzien to 30 s a dzien to 5s 
*/
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import lombok.Getter;
import lombok.Setter;
import managementStrategy.impl.ManagementStrategyManager;
import managment.impl.Managment;
import producer.impl.Producer;
import algorithms.AlgorithManager;

import advertisingDepartment.impl.AdvertisingDepartment;
import advertisingStrategy.impl.AdvertisingStrategyManager;
/**
 * Panel informacyjny o wybranym producencie
 * @author Witold Muszynski
 *
 */
@SuppressWarnings("serial")
public class ProducerInfoPanel extends JPanel {
	static final Logger log = Logger.getLogger(ProducerInfoPanel.class);
	 private javax.swing.JButton jButton1;
	    private javax.swing.JButton changeAlg;
	    private javax.swing.JComboBox ordersComboBox;
	    private javax.swing.JComboBox algoritmComboBox;
	    @Getter
	    private javax.swing.JComboBox productNameLabel;
	    private javax.swing.JLabel jLabel1;
	    @Getter
	    private javax.swing.JLabel companyNameLabel;
	    @Getter
	    private javax.swing.JLabel categoryLabel;
	    @Getter
	    private javax.swing.JLabel priceValueLabel;
	    @Getter
	    private javax.swing.JLabel timeValueLabel;
	    private javax.swing.JLabel jLabel2;
	    private javax.swing.JLabel jLabel3;
	    private javax.swing.JLabel jLabel4;
	    private javax.swing.JLabel jLabel5;
	    private javax.swing.JLabel jLabel6;
	    private javax.swing.JLabel jLabel7;
	    private javax.swing.JLabel jLabel8;
	    private javax.swing.JLabel jLabel9;
	    @Getter
	    private javax.swing.JList orderList;
	    private javax.swing.JPanel jPanel1;
	    private javax.swing.JPanel jPanel2;
	    private javax.swing.JPanel jPanel3;
	    private javax.swing.JPanel jPanel5;
	    private javax.swing.JPanel jPanel6;
	    private javax.swing.JScrollPane jScrollPane2;
	    private Producer producer;
	    private HistoryDialog historyDialog;
	    private MagazinStateDialog magazinStateDialog;
	    
	    private javax.swing.JButton magazinStateButton;
	    private javax.swing.JLabel magazinStateLabel;

	private javax.swing.JButton changeAdvertisingStrategy;
    private JComboBox<String> advertisingStrategies;
	private JLabel advertisingStrategyLabel;
	
	private javax.swing.JButton showSalesHistory;
    private JComboBox<String> salesTime;
	private JLabel showSalesHistoryLabel;

	private javax.swing.JButton changeManagementStrategy;
    private JComboBox managementStrategies;
	private JLabel managementStrategyLabel;
	
	private javax.swing.JButton showAdvHistoryButton;
    private JComboBox showAdvHistoryComboBox;
	private JLabel showAdvHistoryLabel;
	
	
	private SalesHistoryDialog salesHistoryDialog;
	private AdvertisingHistoryDialog advHistoryDialog;
	public ProducerInfoPanel(HistoryDialog historyDialog, MagazinStateDialog magazinStateDialog,SalesHistoryDialog sales,AdvertisingHistoryDialog ads) {
		this.historyDialog = historyDialog;
		this.magazinStateDialog = magazinStateDialog;
		this.salesHistoryDialog = sales;
		this.advHistoryDialog = ads;
		companyNameLabel = new JLabel();
		categoryLabel = new JLabel();
		init();
		addLabel();
	
	}
	
	public void setProducer(Producer producer) {
		this.producer = producer;

		if(producer!=null) {
			historyDialog.setProducer(producer);
			magazinStateDialog.setProducer(producer);
			try {
				String advertisingStrategyName = producer.getManagment().getAdvDepartment().getManager().getCurrentStrategyName();
				advertisingStrategies.setSelectedItem(advertisingStrategyName);
			} catch(NullPointerException npe) {
				log.error("Nie udało się pobrać strategii marketingowej dla producenta " + producer.getCompanyName());

			}
			try {
				String managementStrategyName = producer.getManagment().getStrategyManager().getCurrentStrategyName();
				managementStrategies.setSelectedItem(managementStrategyName);
			} catch(NullPointerException npe) {
				log.error("Nie udało się pobrać strategii zarządu dla producenta " + producer.getCompanyName());				

			}
		}
	}
	
	private void init() {
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        companyNameLabel = new javax.swing.JLabel();
        categoryLabel = new javax.swing.JLabel();
        priceValueLabel = new javax.swing.JLabel();
        timeValueLabel = new javax.swing.JLabel();
        productNameLabel = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        ordersComboBox = new javax.swing.JComboBox();
        algoritmComboBox = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        changeAlg = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        orderList = new javax.swing.JList();

        magazinStateLabel = new javax.swing.JLabel();
        magazinStateButton = new javax.swing.JButton();
        
        advertisingStrategyLabel = new JLabel();
        advertisingStrategies = new javax.swing.JComboBox<String>();
        changeAdvertisingStrategy = new javax.swing.JButton();

        managementStrategyLabel = new JLabel();
        managementStrategies = new javax.swing.JComboBox<String>();
        changeManagementStrategy = new javax.swing.JButton();
        
    	showSalesHistory = new javax.swing.JButton();
        salesTime = new javax.swing.JComboBox();
    	showSalesHistoryLabel= new JLabel();
    	
    	showAdvHistoryButton = new javax.swing.JButton();
        showAdvHistoryComboBox = new javax.swing.JComboBox();
    	showAdvHistoryLabel= new JLabel();
    }


	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

	}
	
	private void addLabel(){
	        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14));
	        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
	        jLabel6.setText("Opis firmy");

	        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel1.setText("Nazwa firmy:");

	        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel2.setText("Kategoria:");

	        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel3.setText("Produkty:");

	        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel5.setText("Aktualna cena:");

	        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel7.setText("Czas produkcji 1 jedn.:");

	        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
	        jPanel2.setLayout(jPanel2Layout);
	        jPanel2Layout.setHorizontalGroup(
	            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
	            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
	            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
	            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
	        );
	        jPanel2Layout.setVerticalGroup(
	            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel2Layout.createSequentialGroup()
	                .addComponent(jLabel1)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jLabel2)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(jLabel3)
	                .addGap(13, 13, 13)
	                .addComponent(jLabel5)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jLabel7)
	                .addContainerGap(18, Short.MAX_VALUE))
	        );

	        companyNameLabel.setText("--");

	        categoryLabel.setText("--");

	        priceValueLabel.setText("--");

	        timeValueLabel.setText("--");

	        productNameLabel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

	        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
	        jPanel3.setLayout(jPanel3Layout);
	        jPanel3Layout.setHorizontalGroup(
	            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(companyNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
	            .addComponent(categoryLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
	            .addGroup(jPanel3Layout.createSequentialGroup()
	                .addComponent(timeValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
	                .addContainerGap())
	            .addGroup(jPanel3Layout.createSequentialGroup()
	                .addComponent(productNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap())
	            .addGroup(jPanel3Layout.createSequentialGroup()
	                .addComponent(priceValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
	                .addContainerGap())
	        );
	        jPanel3Layout.setVerticalGroup(
	            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel3Layout.createSequentialGroup()
	                .addComponent(companyNameLabel)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(categoryLabel)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(productNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(11, 11, 11)
	                .addComponent(priceValueLabel)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(timeValueLabel)
	                .addGap(19, 19, 19))
	        );


	        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        jLabel4.setText("Historia zamowien:");
	        jLabel8.setText("Algorytm negocjacji:");
	        advertisingStrategyLabel.setText("Strategia marketingowa:");
	        managementStrategyLabel.setText("Strategia zarz�du:");
	        magazinStateLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
	        magazinStateLabel.setText("Stan magazynu:");

	        ordersComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dzienna", "Tygodniowa", "Miesieczna", "Roczna" }));

	        algoritmComboBox.setModel(new javax.swing.DefaultComboBoxModel(AlgorithManager.getAllAlgorithm()));

	        String[] _advertisingStrategies = AdvertisingStrategyManager.getStrategiesNames();
	        advertisingStrategies.setModel(new javax.swing.DefaultComboBoxModel(_advertisingStrategies));
	      
	        String[] _managementStrategies = ManagementStrategyManager.getStrategiesNames();
	        managementStrategies.setModel(new javax.swing.DefaultComboBoxModel(_managementStrategies));
	        
	        jButton1.setText("Generuj");
	        jButton1.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                jButton1ActionPerformed(evt);
	            }
	        });
	        showSalesHistoryLabel.setText("Sprzeda�");
	        salesTime.setModel( new javax.swing.DefaultComboBoxModel(new String[] { "Dzienna", "Tygodniowa", "Miesieczna", "Roczna" }));
	        showSalesHistory.setText("Wy�wietl");
	        showSalesHistory.addActionListener(new ActionListener(){

	        	@Override
				public void actionPerformed(ActionEvent e) {
					salesHistoryDialog.setProducer(producer);
					salesHistoryDialog.setLabelText( salesTime.getSelectedItem().toString() + " historia sprzeda�y ");
					salesHistoryDialog.setHistory(salesTime.getSelectedItem().toString());
					salesHistoryDialog.repaint();
					salesHistoryDialog.setVisible(true);
				}
	        	
	        });
	        
	        showAdvHistoryLabel.setText("Reklama");
	        showAdvHistoryComboBox.setModel( new javax.swing.DefaultComboBoxModel(new String[] { "Dzienna", "Tygodniowa", "Miesieczna", "Roczna" }));
	        showAdvHistoryButton.setText("Wy�wietl");
	        showAdvHistoryButton.addActionListener(new ActionListener(){

	        	@Override
				public void actionPerformed(ActionEvent e) {
	        		advHistoryDialog.setProducer(producer);
	        		advHistoryDialog.setLabelText( showAdvHistoryComboBox.getSelectedItem().toString() + " historia reklamy ");
	        		advHistoryDialog.setHistory(showAdvHistoryComboBox.getSelectedItem().toString());
	        		advHistoryDialog.repaint();
	        		advHistoryDialog.setVisible(true);
				}
	        	
	        });
	        
	        changeAlg.setText("Zmien");
	        changeAlg.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String value = (String) algoritmComboBox.getSelectedItem();
					AlgorithManager.changeAlg(value);
				}
			});
	        
	        magazinStateButton.setText("Wy�wietl");
	        magazinStateButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
//					magazinStateDialog.setProducer(producer);
					magazinStateDialog.repaint();
					magazinStateDialog.setVisible(true);
				}
			});
	        
	        changeAdvertisingStrategy.setText("Zmie�");
	        changeAdvertisingStrategy.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(producer==null) return;
					String strategy = (String) advertisingStrategies.getSelectedItem();
					producer.getManagment().getAdvDepartment().getManager().changeStr(strategy);
				}
			});

	        changeManagementStrategy.setText("Zmie�");
	        changeManagementStrategy.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(producer==null) return;
					String strategy = (String) managementStrategies.getSelectedItem();
					producer.getManagment().getStrategyManager().changeStr(strategy);
				}
			});
	        
	        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
	        jPanel5.setLayout(jPanel5Layout);
	        jPanel5Layout.setHorizontalGroup(
	            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel5Layout.createSequentialGroup()
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
	                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(magazinStateLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(showSalesHistoryLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(showAdvHistoryLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(advertisingStrategyLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(managementStrategyLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addComponent(algoritmComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(ordersComboBox, 0, 73, Short.MAX_VALUE)
	                    .addComponent(salesTime, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
	                    .addComponent(showAdvHistoryComboBox, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
	                    .addComponent(advertisingStrategies, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(managementStrategies, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addComponent(changeAlg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(showSalesHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(showAdvHistoryButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(magazinStateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(changeAdvertisingStrategy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(changeManagementStrategy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                .addContainerGap(28, Short.MAX_VALUE))
	        );
	        jPanel5Layout.setVerticalGroup(
	            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel5Layout.createSequentialGroup()
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel4)
	                    .addComponent(ordersComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jButton1))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel8)
	                    .addComponent(algoritmComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(changeAlg))
	                    .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(showSalesHistoryLabel)
	                    .addComponent(salesTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(showSalesHistory))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(showAdvHistoryLabel)
	                    .addComponent(showAdvHistoryComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(showAdvHistoryButton))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(advertisingStrategyLabel)
	                    .addComponent(advertisingStrategies, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(changeAdvertisingStrategy))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                	.addComponent(managementStrategyLabel)
	                    .addComponent(managementStrategies, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(changeManagementStrategy))
	                .addGap(18, 18, 18)
	                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(magazinStateLabel)
	                    .addComponent(magazinStateButton))
	                .addContainerGap(21, Short.MAX_VALUE))
	        );

	        jLabel9.setText("Lista produktow:");

	        orderList.setModel(new javax.swing.AbstractListModel() {
	            String[] strings = { "" };
	            public int getSize() { return strings.length; }
	            public Object getElementAt(int i) { return strings[i]; }
	        });
	        jScrollPane2.setViewportView(orderList);

	        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
	        jPanel6.setLayout(jPanel6Layout);
	        jPanel6Layout.setHorizontalGroup(
	            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel6Layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
	                    .addComponent(jLabel9))
	                .addContainerGap())
	        );
	        jPanel6Layout.setVerticalGroup(
	            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel6Layout.createSequentialGroup()
	                .addComponent(jLabel9)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
	        );

	        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	        jPanel1.setLayout(jPanel1Layout);
	        jPanel1Layout.setHorizontalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addContainerGap(20, Short.MAX_VALUE))
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addGap(20, 20, 20))
	        );
	        jPanel1Layout.setVerticalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	        );

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
	        this.setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );

	}
	
private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			historyDialog.setLabelText( ordersComboBox.getSelectedItem().toString() + " historia zamowien ");
			historyDialog.setHistory(ordersComboBox.getSelectedItem().toString());
			historyDialog.repaint();
			historyDialog.setVisible(true);
}

	
}
