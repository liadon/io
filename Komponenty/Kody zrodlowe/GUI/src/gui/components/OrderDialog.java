package gui.components;


import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import negotiator.Negotiator;
import negotiator.OrderBean;
import producer.impl.Producer;
import product.enums.Category;
import product.impl.Product;


@SuppressWarnings("serial")
/**
 * Okno dialogowe dla zamówień
 * @author Witold Muszynski
 *
 */
public class OrderDialog extends JDialog {
	
	
		private javax.swing.JButton jButton1;
	    private javax.swing.JButton jButton2;
	 
	    private javax.swing.JLabel jLabel1;
	    private javax.swing.JLabel jLabel2;
	    private javax.swing.JSpinner jSpinner1;
	
	
	private JComboBox<String> box =new  JComboBox<String>();
	
	private JSpinner countField = new JSpinner(new SpinnerNumberModel(0,0, 10000,1));
	
	private JButton confirm = new JButton("Zatwierdz");
	private JButton cancel = new JButton("Anuluj");
	
	private OrderBean dialogBean = new OrderBean();

	private Producer producer;
	
	public OrderDialog (Frame owner,boolean modal){
		super(owner,modal);
		init();
	}
	
	public void setProducts(List<String> products){
		box.removeAllItems();
		for(String product : products){
			box.addItem(product);
		}
	}
	
	private void init() {
		addComponent();
		bindConfirmButtonActionListener();
		bindCancelButton();
	}
	
	private  OrderBean getData(){
		dialogBean.setProduct(new Product(box.getSelectedItem().toString(), (Integer)jSpinner1.getValue(), Category.PublishingHouse));
		dialogBean.setProducer(this.producer);
		return dialogBean;
	}
	
	private void bindConfirmButtonActionListener(){
		jButton2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//getData();
				producer.reczneprodukcjalol(new Product(box.getSelectedItem().toString(), (Integer)jSpinner1.getValue(), producer.getCategory()));
				closeWindow();
				//Negotiator.addOrder(dialogBean);
				countField.setValue(0);
				
			}
		});
	}
	
	private void bindCancelButton(){
		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				closeWindow();
			}
		});
	}
	
	private void closeWindow(){
		Window window = SwingUtilities.getWindowAncestor(cancel);
		this.dispose();
		this.setVisible(false);
	}
	
	private void addComponent(){
		
		jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        box = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSpinner1 = new javax.swing.JSpinner();


        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(new java.awt.Rectangle(400, 200, 0, 0));
        
        jLabel1.setText("Produkt");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Ilosc");

        box.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton1.setText("Anuluj");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeWindow();
            }
        });
        
        jButton2.setText("Produkuj");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSpinner1)
                            .addComponent(box, 0, 85, Short.MAX_VALUE))))
                .addContainerGap(34, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pack();
	}

	public void setCurrentProducer(Producer producer) {
		this.producer = producer;
		
	}
}
