package gui.main;

import gui.components.AdvertisingHistoryDialog;
import gui.components.HistoryDialog;
import gui.components.MagazinStateDialog;
import gui.components.MenuBar;
import gui.components.OrderDialog;
import gui.components.ProducerInfoPanel;
import gui.components.SalesHistoryDialog;
import gui.components.WaitTimeDialog;
import gui.graph.controller.GraphController;
import gui.graph.controller.GraphListener;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("serial")
/**
 * Okno dla calego programu
 * @author Witold Muszynski
 *
 */
public class Frame extends JFrame {
	OrderDialog dialog;
	HistoryDialog historyDialog;
	MagazinStateDialog magazinStateDialog;
	SalesHistoryDialog salesHistoryDialog;
	AdvertisingHistoryDialog advertisingHsitoryDialog;
	WaitTimeDialog waitTimeDialog;
	ProducerInfoPanel panel;
	MenuBar menuBar;


	
	/*public Frame() {
		dialog = new OrderDialog(this, false);
		menuBar = new MenuBar();
		historyDialog = new HistoryDialog(this, false);
		magazinStateDialog = new MagazinStateDialog(this, false);
		salesHistoryDialog = new SalesHistoryDialog(this,false);
		panel = new ProducerInfoPanel(historyDialog, magazinStateDialog,salesHistoryDialog,advertisingHsitoryDialog);
		waitTimeDialog = new WaitTimeDialog(this, false);
		GraphListener listener = new GraphListener(dialog, panel);*/

	private String getContextFilename() {

		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");
		chooser.setFileFilter(filter);
		chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
	    int returnVal = chooser.showOpenDialog(this);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       	return chooser.getSelectedFile().getName();
	    } else {
	    	return "applicationContext.xml";
	    }
	}
	
	public Frame() {
		dialog = new OrderDialog(this, false);
		menuBar = new MenuBar();
		historyDialog = new HistoryDialog(this, false);
		magazinStateDialog = new MagazinStateDialog(this, false);

		salesHistoryDialog = new SalesHistoryDialog(this,false);
		advertisingHsitoryDialog = new AdvertisingHistoryDialog(this,false);
		panel = new ProducerInfoPanel(historyDialog, magazinStateDialog,salesHistoryDialog,advertisingHsitoryDialog);
		waitTimeDialog = new WaitTimeDialog(this, false);
		GraphListener listener = new GraphListener(dialog, panel);    
		ApplicationContext context = new ClassPathXmlApplicationContext(getContextFilename());
		setLocationByPlatform(true);
		GraphController graphController = (GraphController) context.getBean("graphController");
		graphController.setFrame(this);
		graphController.init(listener);
		add(graphController.getVv());
		menuBar.setNegotiator(graphController.getNegotiator());
		menuBar.setWaitTimeDialog(waitTimeDialog);
		menuBar.setHistoryDialog(historyDialog);
		setJMenuBar(menuBar);
		add(panel, BorderLayout.EAST);
		pack();

	}
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				Frame frame = new Frame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
			
		});
		
	}
	
	
	
}
