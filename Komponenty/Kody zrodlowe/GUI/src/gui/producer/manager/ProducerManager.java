package gui.producer.manager;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import producer.impl.Producer;
/**
 * Klasa odpowiedzialna za zarzadznie
 * wszystkimi producentami
 * @author Przemek Pioro
 *
 */
@Data
public class ProducerManager {
	
	private List<Producer> producers;
	
	public List<Producer> getAllProducer(){
		return new ArrayList<Producer>(producers);
	}
	
}
