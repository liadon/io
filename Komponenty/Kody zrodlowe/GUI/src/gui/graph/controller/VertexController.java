package gui.graph.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import producer.impl.Producer;
@Data
public class VertexController {
	List<Producer> producent;
	
	public VertexController(){
		producent = new ArrayList<Producer>();
	}
	
}
