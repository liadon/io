package gui.graph.controller;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import negotiator.Negotiator;
import producer.bean.ProductRequiredBean;
import producer.impl.Producer;
import product.enums.Category;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import gui.components.HistoryDialog;
import gui.components.MagazinStateDialog;
import gui.components.OrderDialog;
import gui.components.ProducerInfoPanel;

public class GraphListener implements GraphMouseListener<Producer> {

	private JLabel CompanyName;
	private JLabel CompanyCategory;
	private JComboBox ProductNameLabel;
	private JLabel PriceValueLabel;
	private JLabel TimeValueLabel;
	private JList OrderList;
	private OrderDialog dialog;
	private List<String> products;

	private ProductRequiredBean prbTemp;
	private String orderString;
	private List<String> orderListString;
	private ProducerInfoPanel panel;

	public GraphListener(OrderDialog dialog, ProducerInfoPanel panel) {
		this.CompanyName = panel.getCompanyNameLabel();
		this.CompanyCategory = panel.getCategoryLabel();
		this.ProductNameLabel = panel.getProductNameLabel();
		this.TimeValueLabel = panel.getTimeValueLabel();
		this.PriceValueLabel = panel.getPriceValueLabel();
		this.OrderList = panel.getOrderList();
		this.dialog = dialog;
		this.panel = panel;

		this.orderString = "";
		this.orderListString = new ArrayList<String>();

	}

	@SuppressWarnings("unchecked")
	@Override
	public void graphClicked(Producer producer, MouseEvent arg1) {
		panel.setProducer(producer);
		
		CompanyName.setText(producer.getName());
		CompanyName.repaint();

		CompanyCategory.setText(producer.getCategory().toString());
		CompanyCategory.repaint();

		products = producer.getProductNames();
		ProductNameLabel.setModel(new javax.swing.DefaultComboBoxModel(producer
				.getProductNames().toArray()));
		ProductNameLabel.repaint();

		PriceValueLabel.setText(producer.getPrice() +  " zl");
		TimeValueLabel.setText(producer.getProduceTime()+ " h");

		orderListString.clear();

		for (String product : products) {
			orderString = "";

			prbTemp = producer.getProductRequierd(product);
			if (prbTemp != null) {
				orderString = product  +" "+  prbTemp.getName() + " " + prbTemp.getCount();
				orderListString.add(orderString);
			}
			else{
				orderString = product;
				orderListString.add(orderString);
			}
		}
		OrderList.setListData(orderListString.toArray());

		/*if (producer.getCategory().equals(Category.PublishingHouse)) {
			dialog.setCurrentProducer(producer);
			dialog.setVisible(true);
			dialog.setProducts(producer.getProductNames());
			Negotiator.setProducer(producer);
		}
		if (producer.getCategory().equals(Category.PowerPlant)) {*/
			dialog.setCurrentProducer(producer);
			dialog.setVisible(true);
			dialog.setProducts(producer.getProductNames());
			Negotiator.setProducer(producer);
		//}
	}

	@Override
	public void graphPressed(Producer arg0, MouseEvent arg1) {
	}

	@Override
	public void graphReleased(Producer arg0, MouseEvent arg1) {
	}
}
