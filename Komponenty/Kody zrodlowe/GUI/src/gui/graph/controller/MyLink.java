package gui.graph.controller;

import lombok.Data;
import producer.impl.Producer;
@Data
public class MyLink {
	private Producer producer;
	private Producer supplier;

	public MyLink(Producer p1,Producer p2) {
		this.producer = p1;
		this.supplier = p2;
	}

}
