package gui.scheduler;

import gui.producer.manager.ProducerManager;
import lombok.Data;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import producer.impl.Producer;
@Data
/**
 * Klasa uzywana do harmonogramowej produckji
 * przez przedsiebiorstwa
 * @author Przemek Pioro
 *
 */
public class TaskExecutor {
	private Logger log = Logger.getLogger(TaskExecutor.class);
	private ApplicationContext context;
	private ProducerManager producerManager;
	/**
	 * Metoda wywoluje na kazdym przedsiebiorstwie
	 * metode harmonogramowej produkcji
	 */
	public void execute(){
		log.info("Task executor run");	
		for(Producer producer : producerManager.getAllProducer()){
			producer.analyze();
		}
	}
}
