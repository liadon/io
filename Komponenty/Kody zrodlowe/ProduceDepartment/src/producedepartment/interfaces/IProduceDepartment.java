package producedepartment.interfaces;

import producedepartment.bean.ProduceHistory;
import product.impl.Product;
import product.interfaces.IProduct;
/**
 * Interfejs udostepniany przez dzial produkcji
 * @author Przemek Pioro
 *
 */
public interface IProduceDepartment {
	IProduct getProductFromMagazin(Product p);
	Product produce(Product produkt);
	ProduceHistory getProduceHistory();
}
