package producedepartment.impl;

import java.util.AbstractMap;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import lombok.Getter;
import lombok.Setter;

import org.apache.log4j.Logger;

import producedepartment.bean.ProduceHistory;
import producedepartment.interfaces.IProduceDepartment;
import product.impl.Product;
import product.utils.ProductUtils;
import saledepartment.impl.Department;

/**
 * Komponent odpowiedzialny za dzial produkcji przedsiebiorstwa
 * 
 * @author Przemek Pioro
 * 
 */
@Resource
public class ProduceDepartment extends Department implements IProduceDepartment {
	static final Logger log = Logger.getLogger(ProduceDepartment.class);
	@Setter
	private ProduceHistory produceHistory;
	@Getter
	private int requierdProductCount = 0;

	@Override
	/**
	 * Pobiera produkt z magazunu
	 */
	public Product getProductFromMagazin(Product p) {
		for (Product produkt : magazin.getMagazinState()) {
			if (ProductUtils.equals(produkt, p)){
				produceHistory.addProdukt(produkt, new Date());
				magazin.setProductCount(p);
				return p;
			}
		}
		
		return null;
	}
	
	public int getProductCount(Product p){
		return magazin.getProductCount(p);
	}

	@Override
	/**
	 * Produkuje produkt
	 */
	public Product produce(Product product) {//wykorzystywane gdy do produkcji nie potrzeba zadnych polproduktow
		log.debug("Produkuje produkt o parametrach nazwa: " + product.getName() + " ilosc: "+ product.getCount());
		
		produceHistory.addProdukt(product, new Date());
		magazin.addProduct(product);
		log.debug("Obecna ilo�� w magazynie:"+ getProductCount(product));
		return product;

	}
	/**
	 * Zwraca historie produkcji
	 */
	@Override
	public ProduceHistory getProduceHistory() {
		return produceHistory;
	}
	/**
	 * Pobiera liste produktow z magazunu
	 * @return
	 */
	public List<Product> getProductList() {
		return magazin.getMagazinState();

	}
	/**
	 * Sprawdza czy produkt jest dostepny w magazynie
	 * @param p
	 * @return
	 */
	public boolean isProductAvaible(Product p) {
		return magazin.isProductAvaible(p);
		
	}
	/**
	 * Zwieksza liczbe potrzebnych produktow 
	 * w magazynie
	 * @param count
	 */
	public void addBuyProductCount(int count) {
		this.requierdProductCount+=count;
	}

	public boolean isProductAvaible(Product product, int count) {
		// TODO Auto-generated method stub
		return isProductAvaible(product)&& count<=getProductCount(product);
	}
	////////////////////////////2.0
	private LinkedList<Product> neededHalfProducts = new LinkedList<Product>();
	private LinkedList<Entry<Product,Product>> pendingProductionTasks = new LinkedList<Entry<Product,Product>>();
	public void produce(Product product, String requiredProdName, int count) {//wykorzystywane gdy do produkcji potrzebne jakies polprodukty
		Product required = new Product(requiredProdName);
		required.setCount(count);
		log.info("przystepowanie do produkcji "+product.getName()+" w ilosci "+product.getCount());
		log.info("potrzeba "+count+" "+requiredProdName);
		log.info("stan magazynu "+magazin.getProductCount(required));
		
		if(isProductAvaible(required,count)){
			log.debug("Produkuje produkt o parametrach nazwa: " + product.getName() + " ilosc: "+ product.getCount());
			magazin.setProductCount(required);
			produceHistory.addProdukt(product, new Date());
			magazin.addProduct(product);
		}else{
			log.debug("Produkcja : " + product.getName() + "w ilosci: "+ product.getCount()+" wstrzymana, sk�adanie zam�wienia na "+requiredProdName+" w ilo�ci"+count);
			neededHalfProducts.addLast(required);
			pendingProductionTasks.addLast(new AbstractMap.SimpleEntry<Product,Product>(product,required));
		}
		
		//return product;
	}
	public void produce(){//wykorzystywane po kupnie polproduktow
		log.debug("zakupiono p�produkty, weryfikacja mozliwo�ci produkcji");
		int i =0;
		for(i = 0;i<pendingProductionTasks.size();){
			Map.Entry<Product, Product> entry = pendingProductionTasks.get(i);
			Product required = entry.getValue();
			if(isProductAvaible(required,required.getCount())){
				log.debug("Produkuje produkt o parametrach nazwa: " + entry.getKey().getName() + " ilosc: "+ entry.getKey().getCount());
				produceHistory.addProdukt(entry.getKey(), new Date());
				magazin.addProduct(entry.getKey());
				pendingProductionTasks.remove(i);
				magazin.setProductCount(required);
			}else{
				i++;
			}
		}
		
	}
	public List<Product> getOrders() {
		// TODO Auto-generated method stub
		List<Product> toRet = new LinkedList(neededHalfProducts);
		neededHalfProducts.clear(); 
		return toRet;
	}

}
