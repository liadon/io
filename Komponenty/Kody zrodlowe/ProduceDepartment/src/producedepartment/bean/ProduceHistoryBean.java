package producedepartment.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import product.enums.Category;
import product.impl.Product;
/**
 * Bean dla historii produkcji
 * @author Przemek Pioro
 *
 */
public class ProduceHistoryBean extends Product {
	@Getter
	@Setter
	private Date productData;

	public ProduceHistoryBean(String name, int count, Category category,Date date) {
		super(name, count, category);
		setProductData(date);
	}
}
