package producedepartment.bean;

import java.util.Date;
import java.util.LinkedList;

import lombok.Getter;
import product.impl.Product;

/**
 * Przechowuje informacje o wyprodukowanych
 * elementach przez producenta
 * @author Przemek Pioro
 *
 */
public class ProduceHistory {
	@Getter
	LinkedList<ProduceHistoryBean> history;

	public ProduceHistory() {
		history = new LinkedList<ProduceHistoryBean>();
	}
	/**
	 * Dodaje produkt do historii produkcji
	 * @param product
	 * @param date
	 */
	public void addProdukt(Product product, Date date) {
		history.addFirst(new ProduceHistoryBean(product.getName(), product.getCount(), product.getCategory(), date));
	}

	
}
