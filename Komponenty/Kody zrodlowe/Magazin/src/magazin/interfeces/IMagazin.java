package magazin.interfeces;

import java.util.List;

import product.impl.Product;
/**
 * Interfejs udostepniany przez komponent Magazyn
 * @author Przemek Pioro
 *
 */
public interface IMagazin {
	public void addProduct(Product produkt);
	public Product getProduct(Product p);
	public List<Product> getMagazinState();
	public boolean isProductAvaible(Product p);
	public void setStartState(List<Product> products);
	public int getProductCount(Product p);
}
