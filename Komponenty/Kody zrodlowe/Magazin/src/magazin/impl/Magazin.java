package magazin.impl;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;
import magazin.interfeces.IMagazin;
import product.impl.Product;
import product.utils.ProductUtils;

/**
 * Klasa implementujaca komponent Magazyn
 * 
 * @author Przemek Pioro
 * 
 */
public class Magazin implements IMagazin {
	@Setter
	private List<Product> products;

	public Magazin() {
		this.products = new ArrayList<Product>();
	}

	/**
	 * Metoda dodajaca produkt do magazynu
	 */
	@Override
	public void addProduct(Product produkt) {
		boolean exist = false;
		for (Product product : products) {
			if (product.getName().equals(produkt.getName())) {
				//if (produkt.getCategory().equals(product.getCategory())) {
					product.setCount(product.getCount() + produkt.getCount());
					exist = true;
				//}
			}
		}
		if (!exist) {
			products.add(produkt);
		}
	}

	@Override
	public Product getProduct(Product p) {
		for (Product product : products) {
			if (ProductUtils.equals(product, p)) {
				if (product.getCount() >= p.getCount()) {
					return product;
				}
			}
		}
		return null;
	}

	/**
	 * Metoda pobierajaca obecny
	 * stan magazynu
	 */
	@Override
	public List<Product> getMagazinState() {
		return this.products;
	}
	/**
	 * Metoda sprawdzajaca czy dany produkt jest dostepny
	 * w magazynie
	 */
	@Override
	public boolean isProductAvaible(Product p1) {
		for (Product p2 : products) {
			if (ProductUtils.equals(p1, p2)) {
				if (p2.getCount() >= p1.getCount()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void setStartState(List<Product> products) {
		this.products = products;

	}
	/**
	 * Zwraca licznosc danego produktu w magazynie
	 */
	@Override
	public int getProductCount(Product p) {
		for (Product product : products) {
			if (ProductUtils.equals(p, product)) {
				return product.getCount();
			}
		}
		return 0;
	}
	/**
	 * Zmienia licznosc produktu w magazynie
	 * @param p
	 */
	public void setProductCount(Product p) {
		for (Product product : products) {
			if (ProductUtils.equals(p, product)) {//tu mozna wywalic spr po kategorii
				product.setCount(product.getCount() - p.getCount());
			}
		}
	}

}
