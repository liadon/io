package advertisingDepartment.impl;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import product.impl.Product;
import saledepartment.bean.SalesHistory;
import advertisingDepartment.interfaces.IAdvertisingDepartment;
import advertisingStrategy.bean.AdvertisingHistory;
import advertisingStrategy.impl.AdvertisingDecision;
import advertisingStrategy.impl.AdvertisingStrategyManager;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

//@Data
public class AdvertisingDepartment implements IAdvertisingDepartment {
	private AdvertisingHistory adhistory = new AdvertisingHistory();
	//private SalesHistory saleshistory;
	@Getter
	private AdvertisingStrategyManager manager = new AdvertisingStrategyManager();
	//private Producer producer;
	@Override
	public List<AdvertisingDecision> advertise(long funds,SalesHistory saleshistory,List<Product> products) {
		IAdvertisingStrategy strategy = manager.getCurrentStrategy();
		//long money = funds;
		
		//while(true){
			List<AdvertisingDecision> decisions = strategy.find(saleshistory, adhistory, funds,products);
			for(AdvertisingDecision decision:decisions){
				//negotiator.advertise();
				adhistory.addAdvertisment(decision.getAmount(), decision.getProduct(), new Date());
				//Negotiator.advertise(decision, producer);
			}
			
			return decisions;

		//}
		
	}

	@Override
	public AdvertisingHistory getHistory() {
		return adhistory;
	}
	
}
