package advertisingDepartment.interfaces;

import java.util.List;

import product.impl.Product;

import saledepartment.bean.SalesHistory;

import advertisingStrategy.bean.AdvertisingHistory;
import advertisingStrategy.impl.AdvertisingDecision;
import advrtisingStrategy.interfaces.IAdvertisingStrategy;

public interface IAdvertisingDepartment {
	List<AdvertisingDecision> advertise(long funds,SalesHistory saleshistory,List<Product> products);
	AdvertisingHistory getHistory();
}
