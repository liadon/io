package managment.interfeces;

import producedepartment.impl.ProduceDepartment;
import product.impl.Product;
import saledepartment.impl.SaleDepartment;

public interface IManagment {

	Product delegateProduce(Product product);
	int measuredOrder(Product p);
	SaleDepartment getSaleDepartment();
	ProduceDepartment getProduceDepartment();
}
