package managment.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.Getter;
import managementStrategy.impl.ManagementStrategyManager;
import managment.interfeces.IManagment;

import org.apache.log4j.Logger;

import producedepartment.bean.ProduceHistory;
import producedepartment.impl.ProduceDepartment;
import product.enums.Category;
import product.impl.Product;
import saledepartment.bean.SalesHistory;
import saledepartment.impl.SaleDepartment;
import advertisingDepartment.impl.AdvertisingDepartment;
import advertisingStrategy.bean.AdvertisingHistory;
import advertisingStrategy.impl.AdvertisingDecision;
//import advertisingDepartment.impl.AdvertisingDepartment;

/**
 * Komponent odpowiedzialny za zarzadzanie firma
 * @author przemek
 *
 */
@Data
public class Managment implements IManagment {

	static final Logger log = Logger.getLogger(Managment.class);
	@Getter
	private ProduceDepartment produceDepartment;
	@Getter
	private SaleDepartment saleDepartment;
	@Getter
	private AdvertisingDepartment advDepartment;
	private static final int PRODUCT_UNIT_TIME = 5;
	private ManagementStrategyManager strategyManager = new ManagementStrategyManager();
	
	@Override
	/**
	 * Metoda wyceniajaca zamowienie
	 */
	public int measuredOrder(Product p) {
		int productCurrentTime =  produceDepartment.getProductCount(p);
		return p.getCount() - productCurrentTime;
	}

	@Override
	/**
	 * Metoda zwracajacy dany 
	 */
	public Product delegateProduce(Product product) {
		log.info("Realizuje zamowienie dla produktu nazwa: " +product.getName() + "kategoria: " + product.getCount());
		if(isProductAvaible(product)){
			//saleDepartment.sellProduct(product, product.) skad wziac cene? O.o
			return getProductFromMagazin(product);
		}
		else{
			//saleDepartment.sellProduct(product, product.) skad wziac cene? O.o
			return this.produceDepartment.produce(product);
		}
		
	}
	/**
	 * Metoda pobierajaca produkt z magazynu
	 * @param product
	 * @return
	 */
	private Product getProductFromMagazin(Product product){
		return produceDepartment.getProductFromMagazin(product);
	}
	
	public void analyze(String productName, Category category) {//harmonogramowa produkcja z 1.0
		produceDepartment.produce(new Product(productName,PRODUCT_UNIT_TIME,category));
	}
	/**
	 * Metoda sprawdzajaca czy produkt jest dostepny
	 * @param p
	 * @return
	 */
	public boolean isProductAvaible(Product product) {
		return produceDepartment.isProductAvaible(product);
	}
	/**
	 * Dodaje nowego kupione produkty 
	 * potrzebne do produkcji
	 * @param count
	 */
	public void addBuyProductCount(int count) {
		produceDepartment.addBuyProductCount(count);
	}
	/**
	 * Szacuje date zamowienia
	 * @param product
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public Date measuredOrderDate(Product product) {
		Date currentDate = new Date();
		currentDate.setMinutes(currentDate.getMinutes() - produceDepartment.getProductCount(product));
		return currentDate;
	}

	
	private transient List<AdvertisingDecision> ads = new ArrayList<AdvertisingDecision>();
	public void manage(Set<String> products){
		List<Product> prod = new ArrayList<Product>();
		for(String pr : products){
			prod.add(new Product(pr));
		}
		log.info("management ogarnia reklame");
		if(advDepartment!=null) {
			ProduceHistory prodHist = produceDepartment.getProduceHistory();
			SalesHistory salesHistory = saleDepartment.getSalesHistory();
			AdvertisingHistory advHistory = advDepartment.getHistory();
			int funds = strategyManager.getCurrentStrategy().getAdvertismentFunds(prodHist, salesHistory, advHistory);
			ads.addAll(advDepartment.advertise(funds,saleDepartment.getSalesHistory(),prod));
			log.info("stan: "+ads.size()+" reklam");
			if(ads==null||ads.isEmpty()){
				log.error("cos jest nie tak ze strategia reklamy");
			}
		}
		
		//tutaj mozna cos robic ze strategia
	}
	
	public List<AdvertisingDecision> getAdvertisments() {
		List<AdvertisingDecision> ads2;
		log.info("pobieram "+ads.size()+" reklam");
		ads2=new ArrayList<AdvertisingDecision>(ads);
		ads.clear();
		log.info("zwracam "+ads2.size()+" reklam");
		return ads2;
	}

	public boolean isProductAvaible(Product product, int count) {
		return produceDepartment.isProductAvaible(product,count);
	}
	/////////////////////////////////////////////////////////wersja 2.0
	public void analyze(String productName, Category category,String requiredProdName,int count) {//harmonogramowa produkcja z 2.0
		produceDepartment.produce(new Product(productName,PRODUCT_UNIT_TIME,category),requiredProdName,count*PRODUCT_UNIT_TIME);
	}

	public List<Product> getOrders() {
		return produceDepartment.getOrders();
	}

}
