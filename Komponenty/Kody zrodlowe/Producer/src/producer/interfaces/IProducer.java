package producer.interfaces;

import managment.impl.Managment;
import producer.bean.OrderInfo;
import product.enums.Category;
import product.impl.Product;
import product.interfaces.IProduct;

public interface IProducer {
	String getCompanyName();
	Category getCategory();
	Managment getManagment();
	Product executeOrder(String name,int count);
	OrderInfo measuredOrder(String name, int count);
	IProduct buyRequierdProduct(String name,int count,Category category);
	boolean isProductAvaible(Product product);
	boolean isProductAvaible(Product product, int count);
}
