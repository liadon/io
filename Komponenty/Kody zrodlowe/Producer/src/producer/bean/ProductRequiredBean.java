package producer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * Bean z informacja o wymaganych 
 * produktach do produkcji
 * @author przemek
 *
 */
public class ProductRequiredBean {
	private String name;
	private int count;
}
