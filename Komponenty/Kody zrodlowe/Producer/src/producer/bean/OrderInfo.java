package producer.bean;

import java.util.Date;

import lombok.Data;
/**
 * Bean z informacja o zamowieniu
 * @author Przemek Pioro
 *
 */
@Data
public class OrderInfo {
	private Date finishDate;
	//private double cost;
	private long cost;
	private String name;
	private String companyName;
}
