package producer.impl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import advertisingStrategy.impl.AdvertisingDecision;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import managment.impl.Managment;
import producer.bean.OrderInfo;
import producer.bean.ProductRequiredBean;
import producer.interfaces.IProducer;
import product.enums.Category;
import product.impl.Product;
import product.interfaces.IProduct;
@Data

@AllArgsConstructor
@NoArgsConstructor
/**
 * Komponent Producent
 * @author Witold Muszynski
 *
 */
public class Producer implements IProducer {
	static final Logger log = Logger.getLogger(Producer.class);
	private String name;
	private Category category;
	@Getter
	private Managment managment;
	private Map<String,ProductRequiredBean> products;
	private long price;
	private int produceTime;
	@Override
	public String getCompanyName() {
		return name;
	}

	@Override
	public Category getCategory() {
		return category;
	}

	/**
	 * Wycenia zamowienie dla produktu
	 */
	@Override
	public OrderInfo measuredOrder(String name, int count) {
		OrderInfo info = new OrderInfo();
		info.setCompanyName(getCompanyName());
		info.setName(name);
		info.setFinishDate(managment.measuredOrderDate(new Product(name,count,this.category)));
		info.setCost(count *price);
		return info;
	}
	/**
	 * Dodaje nowo kupiony produkt
	 */
	@Override
	public IProduct buyRequierdProduct(String name, int count,	Category category) {
		return new Product(name, count, category);
	}

	@Override//to jest wykorzystywane tylko w wersji 1.0 i jest z dupy
	public Product executeOrder(String name, int count) {
		Product product = new Product(name, count, this.category);
		managment.delegateProduce(product);
		return product;
		
	}
	
	/**
	 * Dodaje nowo zakupione produktu
	 * @param count
	 */
	public void addBuyProductCount(int count){
		this.managment.addBuyProductCount(count);
	}

	public void analyze() {
		managment.manage(products.keySet());
		for(String productName : products.keySet()){
			log.info("zarzad "+name+" dokonuje analizy");
			if(products.get(productName)==null)//nie potrzeba skladnikow
				managment.analyze(productName,getCategory());
			else{//potrzeba skladnikow
				ProductRequiredBean bean = products.get(productName);
				managment.analyze(productName,getCategory(),bean.getName(),bean.getCount());
			}
		}
		//tu bedzie jakis management.manage ktory wykrozysta strategie	
		
	}

	/**
	 * Sprawdza czy jest produkt dostepny
	 */
	@Override
	public boolean isProductAvaible(Product product) {
		return managment.isProductAvaible(product);
	}
	@Override
	public boolean isProductAvaible(Product product,int count) {
		return managment.isProductAvaible(product,count);
	}
	/**
	 * Zwraca liste dostepnych produktow
	 * @return
	 */
	public List<String> getProductNames() {
		return new ArrayList<String>(products.keySet());
	}
	/**
	 * zwraca wymagane produktu do produkcji
	 * @param productName
	 * @return
	 */
	public  ProductRequiredBean getProductRequierd(String productName){
		return products.get(productName);
		
	}

	public List<AdvertisingDecision> getAds() {
		List<AdvertisingDecision> ads = managment.getAdvertisments();
		if(ads==null || ads.isEmpty())
			log	.error("management zwraca brak reklam");
		return ads;
	}
	/////////////////////////////////tylko do neg2
	//public updateOrders
	
	public List<Product> getOrders(){
		return managment.getOrders();
	}
	private long balance = 0;
	public Entry<Product,Long> sell(String name2, int i) {
		// TODO Auto-generated method stub
		log.info(name+" sprzedal "+i+" "+name2);
		Product prod = new Product(name2,i,category);
		long cost = price*i;
		//tu mozna dodac srodki
		balance+=cost;
		managment.getSaleDepartment().sellProduct(prod, (int)cost);
		managment.getSaleDepartment().magazin.setProductCount(prod);
		return new AbstractMap.SimpleEntry<Product,Long>(prod,cost);
	}

	public void buy(Entry<Product,Long> transaction) {
		managment.getSaleDepartment().magazin.addProduct(transaction.getKey());
		managment.getProduceDepartment().produce();//costam sie kupilo to moze od razu cos sie wyprodukowac, czas produkcji jest zerowy dlatego to jest tutaj, rozwazy przeniesienie przy wprwadzaniu czasu produkcji
		balance-=transaction.getValue();
		//tu mozna odjac srodki
	}
	
	@Override
	public boolean equals(Object obj){
		return name.equals(((Producer)obj).name);
	}
	@Override
	public int hashCode(){
		return name.length();
	}

	public void reczneprodukcjalol(Product product) {
		if(products.get(product.getName())==null)//nie potrzeba skladnikow
			managment.getProduceDepartment().produce(product);
		else{//potrzeba skladnikow
			ProductRequiredBean bean = products.get(product.getName());
			managment.getProduceDepartment().produce(product,bean.getName(),bean.getCount()*product.getCount());
		}
		
	}
	
}
