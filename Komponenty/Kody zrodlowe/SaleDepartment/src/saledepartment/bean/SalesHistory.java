package saledepartment.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import product.impl.Product;

@AllArgsConstructor
@NoArgsConstructor
public class SalesHistory {
	@Getter
	List<SellBean> sales = new ArrayList<SellBean>();;
	public void addSellBean(SellBean sbean){
		sales.add(sbean);
	}
	public Product getMaxProfitProduct(){//te funkcyjki zwracaja jakies hardcoded pierdoly, sorka :P
		Collections.sort(sales);
		return sales.get(0).getProduct();
	}
	public Product getMinProfitProduct(){
		Collections.sort(sales);
		return sales.get(sales.size()-1).getProduct();
	}
	public List<Product> getSortedByNewest(){
		Collections.sort(sales);
		ArrayList<Product> lista = new ArrayList<Product>();
		lista.add(sales.get(0).getProduct());
		return lista;// sales.get(0).getProduct();
	}
	public List<Product> getSortedByProfit(){
		Collections.sort(sales);
		ArrayList<Product> lista = new ArrayList<Product>();
		lista.add(sales.get(0).getProduct());
		return lista;
	}
	public Set<Product> getProducts(){
		Set<Product> retSet = new HashSet<Product>();
		for(SellBean bean: sales){
			retSet.add(bean.getProduct());
		}
		return retSet;
	}
}
