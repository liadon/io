package saledepartment.bean;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import product.impl.Product;
/**
 * Bean sprzedazy produktu
 * @author Witold Muszynski
 *
 */
@Data
@AllArgsConstructor
public class SellBean implements Comparable<SellBean> {
	private Product product;
	private int cost;
	private Date date;

	@Override
	public int compareTo(SellBean o) {
		if(this.cost>o.cost)
			return 1;
		else if(this.cost<o.cost)
			return -1;
		else
			return 0;
	}
}
