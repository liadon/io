package saledepartment.interfeces;

import product.impl.Product;
import saledepartment.bean.SalesHistory;
import saledepartment.bean.SellBean;
/**
 * Interfejs udostepniany przez 
 * @author przemek
 *
 */
public interface ISaleDepartment {
	SellBean sellProduct(Product p, int price);
	SalesHistory getSalesHistory();
}
