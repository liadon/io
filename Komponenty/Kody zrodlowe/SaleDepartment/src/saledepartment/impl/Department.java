package saledepartment.impl;

import lombok.Data;
import magazin.impl.Magazin;

/**
 * Abstrakcyjna dla dla wszystkich
 * odzialow firmy
 * @author przemek
 *
 */
@Data
public abstract class Department {
	public Magazin magazin;
}
