package saledepartment.impl;

import java.util.Date;

import product.impl.Product;
import saledepartment.bean.SalesHistory;
import saledepartment.bean.SellBean;
import saledepartment.interfeces.ISaleDepartment;
/**
 * Komponent dzial sprzedazy - 
 * @author przemek
 *
 */
public class SaleDepartment extends Department implements ISaleDepartment  {
	SalesHistory saleshist = new SalesHistory();
	/**
	 * Sprzedaje produkt
	 */
	@Override
	public SellBean sellProduct(Product p,int price) {
		Product product = magazin.getProduct(p);
		SellBean sellBean = new SellBean(product, price,new Date());
		saleshist.addSellBean(sellBean);
		return sellBean;
	}
	@Override
	public SalesHistory getSalesHistory() {
		// TODO Auto-generated method stub
		return saleshist;
	}


}
