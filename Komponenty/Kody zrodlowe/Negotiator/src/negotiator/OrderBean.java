package negotiator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import producer.impl.Producer;
import product.impl.Product;
/**
 * Bean dla zamówienia
 * @author Przemek Pioro
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderBean {
	private Product product;
	private Producer producer;
}
