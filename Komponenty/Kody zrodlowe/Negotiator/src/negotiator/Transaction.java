package negotiator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import producer.impl.Producer;
import product.impl.Product;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
	private Producer buyer;
	private Producer seller;
	private Product product;
	private Long price;
}
