
package negotiator;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Data;
import lombok.Getter;

import org.apache.log4j.Logger;

import producer.bean.OrderInfo;
import producer.bean.ProductRequiredBean;
import producer.impl.Producer;
import product.impl.Product;
import advertisingStrategy.impl.AdvertisingDecision;
import algorithms.AlgorithManager;
/**
 * Globalny negocjator dla producentow
 * @author Przemek Pioro
 *
 */
@Data
public class Negotiator2 extends Thread {
	private Frame frame;
	private List<Producer> producers;
	static final Logger log = Logger.getLogger(Negotiator2.class);
	private static LinkedList<OrderBean> orders = new LinkedList<OrderBean>();
	private static LinkedList<OrderBean> pendingorders = new LinkedList<OrderBean>();
	private Map<String, List<OrderBean>> orderMap = new HashMap<String, List<OrderBean>>();
	private Map<Producer,OrderInfo> negotiationInfo = new HashMap<Producer,OrderInfo>();
	private static Producer mainProducer;//really a buyer
	private static int WAIT_TIME = 10000;
	@Getter
	private List<Transaction> transactions = new LinkedList<Transaction>();

	//private static Map<Product,Set<Advertisment>> advertisments = new HashMap<Product,Set<Advertisment>>();
	private static Map<String,Map<Producer,Long>> advertisments = new HashMap<String,Map<Producer,Long>>();
	@Override
	public void run() {
		while (true) {
			boolean success = false;
			//WaitForOrders();
			collectOrders();
			//pendingorders.addAll(orders);/////
			while (!orders.isEmpty()) {
				//analizeOrders();
				OrderBean ord = orders.removeFirst();/////
				String productName = ord.getProduct().getName();/////
				mainProducer = ord.getProducer();////really a buyer
				//for (String productName : orderMap.keySet()) {
				//	log.info("Analizuje zamowienia dla przedmiotu. nazwa: " + productName);
					///int count = getProductCount(productName);
				/*try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				int count = ord.getProduct().getCount();
					Producer seller = find(/*mainProducer.getProductRequierd(productName)*/new ProductRequiredBean(productName,count),count);
					if (seller !=null){
						Entry<Product,Long> transaction = seller.sell(/*mainProducer.getProductRequierd(productName).getName()*/ord.getProduct().getName(),count/**mainProducer.getProductRequierd(productName).getCount()*/);
						mainProducer.buy(transaction);
						log.info("dokonano transakcji");
						transactions.add(new Transaction(mainProducer,seller,transaction.getKey(),transaction.getValue()));
						success = true;
						log.info("ilosc pozostalych zamowien: "+orders.size()+pendingorders.size());
					}else{
						pendingorders.addLast(ord);
					}

				//}
			}
			
			orders.addAll(pendingorders);
			pendingorders.clear();
			if(!success){
				//jak nie udalo sie dobic targu w zadnym przypadku to jedziemy nastepny step zeby cos sie zmienilo
				log.info("Nie udalo sie dobic targu, nastepny step");
				for(Producer producer : producers){
					producer.analyze();
				}
				log.info("Waiting for orders");
				WaitForOrders();
			}
			success = false;
		}
	}
	
	private void collectOrders() {
		for(Producer producer : producers){
			for(Product prod : producer.getOrders()){
				orders.add(new OrderBean(prod,producer));
			}
		}
	}

	private Producer  find(ProductRequiredBean required,int count){
		if(required !=null){
			for(Producer producer : producers){
				if(producer.getProductNames().contains(required.getName())){
					//log.info("Negocjuje z producentem nazwa: "+producer.getName());
					OrderInfo value = producer.measuredOrder(required.getName(),required.getCount()/**count*/);
					negotiationInfo.put(producer, value);
					//aktualizacja reklam
					for(AdvertisingDecision decision  : producer.getAds()){
						advertise(decision,producer);
						log.info("producent "+producer.getCompanyName()+" reklamuje produkt "+decision.getProduct().getName()+" na kwote "+decision.getAmount());
					}
					/*
					 
					 */
				}
			}
			
			//if(advertisments.isEmpty()){
			//	log.warn("brak reklam");
			//}
			log.info("Reklamy produktu. "+ required.getName());
			if(advertisments.containsKey(required.getName())){
				for(Producer pr:advertisments.get(required.getName()).keySet()){
					log.info(pr.getCompanyName() + " " + advertisments.get(required.getName()).get(pr));
				}
			}
			Producer ret = AlgorithManager.analizeWithAds(advertisments.get(required.getName()),negotiationInfo);
			if(ret == null)
				log.error("nie znajduje zadnego mozliwego producenta");
			if(ret!=null &&ret.isProductAvaible(new Product(required.getName()),required.getCount()/**count*/)){
				log.info("Znalazlem producenta. "+ ret.getCompanyName() +" Ko�cz� dzia�anie");
				return ret;
			//for(Producer pr:){}
			}
			log.info("Chwilowy brak odpowiedniego producenta");
			//return ret;
		}
		return null;
	}	
	
	/**
	 * Analizuje zamowiania od producentow
	 */
	private void analizeOrders() {
		orderMap.clear();
		for (int i = 0; i < orders.size(); i++) {
			OrderBean bean = orders.removeFirst();
			pendingorders.add(bean);
			if (mainProducer == null) {
				mainProducer = bean.getProducer();
			}
			if (!orderMap.containsKey(bean.getProduct().getName())) {
				List<OrderBean> list = new ArrayList<OrderBean>();
				list.add(bean);
				orderMap.put(bean.getProduct().getName(), list);
			} else {
				orderMap.get(bean.getProduct().getName()).add(bean);
			}

		}
		
	}
	

	private int getProductCount(String productName) {
		int count = 0;
		for (OrderBean bean : orderMap.get(productName)) {
			count += bean.getProduct().getCount();
		}
		return count;

	}
	
	public static void setProducer(Producer p){
		 mainProducer = p;
	}
	
	/**
	 * Dodaje zamowienie do kolejki
	 * @param order
	 */
	public static synchronized void addOrder(OrderBean order) {
		orders.addLast(order);
	}
	
	public static synchronized void advertise(final AdvertisingDecision decision,final Producer producer){
		//if(advertisments.containsKey(decision.getProduct())){
			//Set<Advertisment> ads = advertisments.get(decision.getProduct());
			Product toAdvertise = decision.getProduct();
			
			Map<Producer,Long> ads= advertisments.get(toAdvertise.getName());//new HashMap<Producer,Long>();
			if(ads!= null){
				if(ads.containsKey(producer)){
					//log.info("ponowna reklama");
					ads.put(producer,ads.get(producer)+decision.getAmount());//jak jest to update
					return;
				}else{
					ads.put(producer,decision.getAmount());
				}
			}else{
				ads = new HashMap<Producer,Long>();
				ads.put(producer,decision.getAmount());
			}
			advertisments.put(toAdvertise.getName(), ads);
				//ads.add(new Advertisment(producer,decision.getAmount()));
				//jeszcze put?
		//}
			
					//)(decision.getProduct(), new Advertisment(producer,advertisments.get(decision.getProduct()).getFunds()+decision.getAmount()));
		/*}else{
			Set<Advertisment> ads = new HashSet<Advertisment>();
			ads.add(new Advertisment(producer,decision.getAmount()));
			advertisments.put(decision.getProduct(), ads);
		}*/
	}
	/**
	 * Metoda oczekujaca na zamowienia
	 */
	private void WaitForOrders() {
		try {
			Thread.sleep((long) (WAIT_TIME));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Zmienia czas oczekiwania
	 * @param time
	 */
	public static void changeWaitTime(int time){
		log.info("changed wait time");
		WAIT_TIME = time*1000;
	}
}
